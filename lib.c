#include "lib.h"

/** STRING **/

void hexPrint(char* a, FILE *pFile) {
    int i = 0;
    while (a[i] != 0) {
        fprintf(pFile, "%02hhx", a[i]);
        i++;
    }
    fprintf(pFile, "00");
}

/** Lista **/

void listRemove(list_t* l, void* data, funcCmp_t* fc, funcDelete_t* fd){
    listElem_t* iteradorABorrar = l->first;
    while (iteradorABorrar != NULL){
        listElem_t* elQueBorro= iteradorABorrar;
        iteradorABorrar= iteradorABorrar ->next;
        if ( (*fc)(elQueBorro->data, data) == 0){
            if(elQueBorro== l->first){
                listRemoveFirst(l,fd);
                continue;
            } 
            if (elQueBorro == l->last){
                listRemoveLast(l,fd);
                continue;
            }
            if(fd != 0){
                (*fd)(elQueBorro->data);
            }
            (elQueBorro->prev)->next = elQueBorro->next;
            (elQueBorro->next)->prev =elQueBorro->prev;
            free(elQueBorro);
        }
        
        
    }       
}   

void listRemoveFirst(list_t* l, funcDelete_t* fd){
    if (l->first!=NULL){
        if (fd != 0){  
            (*fd)((l->first->data));
        }
        if(l->first->next == NULL){
            free(l->first);
            l->first= NULL;
            l->last =NULL;
        }else{
            listElem_t* aborrar= l->first;
            l->first->next->prev=NULL;
            l->first= l->first->next;
            free(aborrar);
        }
    }
}

void listRemoveLast(list_t* l, funcDelete_t* fd){
    if (l->first!=NULL){
        if (fd != 0){  
            (*fd)((l->last->data));
        }
        if(l->last->prev == NULL){
            free(l->last);
            l->first= NULL;
            l->last =NULL;
        }else{
            listElem_t* aborrar= l->last;
            l->last->prev->next=NULL;
            l->last= l->last->prev;
            free(aborrar);
        }
    }
}
