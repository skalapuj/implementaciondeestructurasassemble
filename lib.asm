%define NULL 0 
%define sizeChar 1
exernt:
    extern malloc
    extern free
    extern fprintf
    extern listRemove
    extern listRemoveLast
    extern listRemoveFirst
    extern fputs
section .data
    formato db '%p' , 0
    coma db ',', 0
    corchete1 db'[' , 0
    corchete2 db ']', 0
    endLine db 10, 0
    formatoIndice db '%u = ', 0
    lista_vacia  db 'NULL',  0

section .text   
funcionesGlobales:
    global strLen   
    global strClone 
    global strCmp
    global strConcat
    global strDelete
    global strPrint
    global listNew
    global listAddFirst
    global listAddLast
    global listAdd
    global listDelete
    global listPrint
    global sorterNew
    global sorterAdd
    global sorterRemove
    global sorterGetSlot
    global sorterGetConcatSlot
    global sorterCleanSlot
    global sorterDelete
    global sorterPrint
    global fs_sizeModFive
    global fs_firstChar
    global fs_bitSplit

FuncionesString:
    strClone:
        PUSH RBP
        MOV RBP, RSP
        PUSH R12
        SUB  RSP , 8
        %define string1 R12
        %define index RSI
        %define elValor DL ;funcion
        
        mov string1, rdi
        call strLen ;pone en EAX el size de a 
        AND RAX, 0x0000FFFF
        
          
        lea RDI, [RAX+1] ;le pido EAX B de memria a maloc
        call malloc
        MOV index, 0

        ciclostrClone:
            CMP byte [string1 + index * sizeChar], 0
            JE finstrClone
            MOV elValor, [string1 + index * sizeChar]
            MOV [RAX + index * sizeChar], elValor
            INC index
            JMP ciclostrClone
        finstrClone:
        mov byte [rax+index*sizeChar], 0
        ADD RSP, 8
        POP R12
        POP RBP
        RET

    strLen:
        ;en RDI recibo el char*
        ;acomodar la pila
        %define indice1 RSI
        %define tamano1 EAX 
        %define elementoiesimo DL
        push rbp
        mov rbp, rsp

        ;funcion
        mov tamano1, 0 ;size del string
        mov indice1, 0

        ciclostrLen:
            MOV elementoiesimo,[RDI + indice1*1]
            cmp elementoiesimo,  0
            je finstrLen
            inc indice1
            inc tamano1
            jmp ciclostrLen

        finstrLen: 
        pop rbp
        ret

    strCmp:
        PUSH RBP
        MOV RBP, RSP
        %define stri RDI
        %define strii RSI
        %define indice R8
        %define elem1 CL
        %define elem2 DL
        MOV indice, 0
        MOV EAX, 0
        ciclostrCmp:
            MOV elem1, [stri + indice*sizeChar]
            MOV elem2, [strii + indice*sizeChar]
            CMP elem1, 0
            JE  primVacio
            CMP elem2, 0
            JE  segVacio
            CMP elem1, elem2
            JG str1Mayor
            JL str1Menor
            INC indice
            JMP ciclostrCmp
        str1Menor:
            MOV EAX, 1
            JMP finCmp
        str1Mayor:
            MOV EAX, -1
            JMP finCmp
        primVacio:
            CMP elem2, 0
            JE finCmp
            JMP str1Menor
        segVacio:
            CMP elem1, 0
            JE finCmp
            JMP str1Mayor
        finCmp:
            POP RBP
            RET

    strConcat:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push rbx
        push r14

        %define str1 r12
        %define str2 r13
        %define tamano rbx
        %define charNuevo r14
        %define indice r8
        %define aCopiar rcx
        %define indiceNuevo r9
        %define elValor1 dl
    

        ;funcion
        ;calculo la cantidad de memoria que voy a neceistar
        mov str1, rdi
        mov str2, rsi
        call strLen
        AND RAX, 0x0000ffff
        mov tamano, rax
        mov rdi, str2
        call strLen
        AND RAX, 0x0000ffff
        add tamano, rax
        lea rdi, [tamano + 1]
        call malloc
        mov charNuevo, rax
        MOV indice, 0
        MOV indiceNuevo,0
        MOV aCopiar, str1
        MOV rbx, 0
        cicloCopia:
            CMP byte [aCopiar + indice * sizeChar], 0
            JE copioSig
            MOV elValor1, [aCopiar + indice * sizeChar]
            MOV [charNuevo + indiceNuevo * sizeChar], elValor1
            INC indice
            INC indiceNuevo
            JMP cicloCopia
        copioSig:
        CMP rbx, 1
        JE liberarMemoria
        MOV indice,0
        MOV rbx, 1
        MOV aCopiar, str2
        JMP cicloCopia
        liberarMemoria: 
        mov byte [charNuevo+indiceNuevo*sizeChar], 0
        cmp str1, str2
        je liberoUnoSolo
        MOV rdi, str1
        call free
        MOV rdi, str2
        call free
        jmp finstrConcat
        liberoUnoSolo:
        MOV rdi, str1
        call free
        finstrConcat:
        MOV RAX, charNuevo
        POP r14
        pop rbx
        pop r13
        pop r12
        pop rbp
        ret
    
    strDelete:
        PUSH RBP
        MOV RBP, RSP
        
        call free
    
        pop rbp
        ret

    strPrint:
        PUSH RBP 
        MOV RBP, RSP

        %define palabra rcx
        %define archivo1 rdx

        MOV palabra, rdi 
        MOV archivo1, rsi
        CMP byte [palabra], 0
        JE escribirNull
        MOV rdi, palabra
        escribir:
        MOV rsi, archivo1
        call fputs
        POP RBP
        RET
        escribirNull:
        MOV rdi, lista_vacia
        jmp escribir
FuncionesListas:

    %define offset_data 0
    %define offset_next 8
    %define offset_prev 16

    %define offset_first 0
    %define offset_last 8


    listNew:
        push rbp
        mov rbp, rsp

        mov rdi, 16
        call malloc
        
        mov qword [rax + offset_first], NULL
        mov qword [rax+ offset_last], NULL

        pop rbp
        ret
         
    listAddFirst:
        push rbp
        mov rbp, rsp
        push rbx
        push r12

        %define punteroLista rbx
        %define data r12
        
        MOV punteroLista, rdi
        MOV data, rsi
        MOV rdi, 24
        call malloc

        %define nodoNuevo rax
        %define primerNodo rdi
        ;me fijo si la lista esta vacia
        MOV primerNodo, [punteroLista + offset_first] ;le pongo el puntero del primer nodo
        MOV [nodoNuevo + offset_data ], data
        MOV [nodoNuevo + offset_next], primerNodo
        MOV qword [nodoNuevo + offset_prev], NULL
        ;tengo el nodo lsito basta con acomodar los punteros 
        cmp qword primerNodo, NULL
        je listaVacia
        mov [primerNodo + offset_prev], nodoNuevo
        mov [punteroLista+ offset_first], nodoNuevo
        jmp finlistAddFirst
        listaVacia:
        mov [punteroLista+offset_first], nodoNuevo
        mov [punteroLista+offset_last], nodoNuevo
        finlistAddFirst:
        pop r12
        pop rbx
        pop rbp
        ret
    listAddLast:
        push rbp
        mov rbp, rsp
        push rbx
        push r12
        %define punteroLista rbx
        %define data r12
        
        MOV punteroLista, rdi
        MOV data, rsi
        MOV rdi, 24
        call malloc

        %define nodoNuevo rax
        %define ultimoNodo rdi
        MOV ultimoNodo, [punteroLista+offset_last] ;le pongo el puntero del ultimo nodo
        MOV [nodoNuevo + offset_data], data
        MOV qword [nodoNuevo + offset_next], NULL
        MOV qword [nodoNuevo + offset_prev], ultimoNodo
        ;tengo el nodo lsito basta con acomodar los punteros 
        cmp qword ultimoNodo, NULL
        je listaVacia2
        mov [ultimoNodo + offset_next], nodoNuevo
        mov [punteroLista + offset_last], nodoNuevo
        jmp finlistAddLast
        listaVacia2:
            mov [punteroLista + offset_last], nodoNuevo
            mov [punteroLista + offset_first], nodoNuevo
        finlistAddLast:
        pop r12
        pop rbx
        pop rbp
        ret
    listAdd:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push r14
        push rbx

        %define criterioCom r13
        %define data r12
        %define punteroLista rbx
        %define punteroIterador r14

        MOv data, rsi
        MOV punteroLista, rdi
        MOV criterioCom, rdx


        MOV punteroIterador, [punteroLista + offset_first]
        ciclolistAdd:
            CMP qword punteroIterador, NULL
            JE termineLaLista
            MOV rdi, [punteroIterador + offset_data]
            MOV rsi, data
            call criterioCom
            CMP EAX, -1
            JE encontreLugar
            MOV punteroIterador, [punteroIterador +offset_next]
            JMP ciclolistAdd
        termineLaLista:
            MOV rdi, punteroLista
            MOV rsi, data
            call listAddLast
            jmp finlistAdd
        encontreLugar:
            %define nodoAnterior rdi
            %define nodoNuevo rax
            
            cmp qword [punteroIterador+ offset_prev], NULL 
            je agregoAlPrincipio
            ;me armo el nodo
            MOV RDI, 24
            call malloc
            ;acomodo los puntero en el nodo
            MOV [nodoNuevo], data
            MOV [nodoNuevo + offset_next], punteroIterador
            MOV nodoAnterior, [punteroIterador+ offset_prev]
            MOV [nodoNuevo + offset_prev], nodoAnterior
            ;coloco el anterior al nuevo apuntando al nuevo
            MOV [nodoAnterior+ offset_next], nodoNuevo
            ; al siguiente le digo que el anterior es el nuevo
            MOV [punteroIterador + offset_prev], nodoNuevo
            jmp finlistAdd

        agregoAlPrincipio:
            MOV rdi, punteroLista
            MOV rsi, data
            call listAddFirst
        finlistAdd:
        POP rbx
        POP r14
        pop r13
        pop r12
        pop rbp
        ret
            


        

    
    
    
    listClone:
        push rbp
        mov rbp, rsp
        sub rsp, 8
        push r13
        push r14
        push rbx

        %define punteroLista rbx
        %define funcionQueCopia r13
        %define listaNueva r14
        

        MOV punteroLista, rdi
        MOV funcionQueCopia, rsi

        call listNew
        mov listaNueva, rax
        MOV punteroLista, [punteroLista + offset_first]
        cmp punteroLista, NULL
        JE finlistClone
            
        ciclolistClone:
            CMP qword punteroLista, NULL
            je finlistClone

            ;copio el data
            MOV rdi, [punteroLista + offset_data]
            call funcionQueCopia
            
            mov rdi, listaNueva
            mov rsi, rax
            call listAddLast

            MOV punteroLista, [punteroLista + offset_next]
           
            jmp ciclolistClone
        finlistClone:
        MOV rax, listaNueva
        pop rbx
        pop r14
        pop r13 
        add rsp, 8
        pop rbp
        ret

    listDelete:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push r14
        sub rsp, 8

        %define funcionQueBorra r13
        %define nodoSig r14

        MOV funcionQueBorra, rsi
        mov nodoSig, [rdi + offset_first]
        ;borro la estructura list de la lista que esta en rdi 
        call free 
        jmp ciclolistDelete
        ;borro nodo por nodo
        ciclolistDelete:
            cmp nodoSig, NULL 
            je finlistDelete
            cmp funcionQueBorra, 0
            je borroNodo
            mov rdi, [nodoSig + offset_data]
            call funcionQueBorra
            borroNodo:
            mov r12, [nodoSig+ offset_next]
            mov rdi, nodoSig
            call free
            mov nodoSig, r12
            jmp ciclolistDelete
        finlistDelete:
        add rsp, 8
        pop r14
        pop r13 
        pop r12
        pop rbp
        ret
    listPrint:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push rbx
        sub rsp, 8

        %define punteroLista rbx
        %define funcionQueCopia r13
        %define archivo r14

        mov punteroLista, rdi
        mov archivo, rsi
        mov funcionQueCopia, rdx
        mov punteroLista, [punteroLista + offset_first]
        
        mov rdi, archivo
        mov rsi, corchete1
        call fprintf
        
        cmp punteroLista, NULL 
        je finlistPrint
        cmp funcionQueCopia, NULL 
        jne ciclolistPrint
        je cicloprintfd
        cicloprintfd: 
            mov rdi, archivo
            mov rsi, formato
            mov rdx, [punteroLista + offset_data]
            call fprintf
            mov punteroLista, [punteroLista + offset_next]
            mov rdi, archivo
            cmp punteroLista, NULL
            je finlistPrint
            mov rsi, coma
            call fprintf
            jmp cicloprintfd
        ciclolistPrint:
            cmp punteroLista, NULL 
            je finlistPrint
            mov rdi, [punteroLista + offset_data]
            mov rsi, archivo
            call funcionQueCopia
            mov punteroLista, [punteroLista + offset_next]
            mov rdi, archivo
            cmp punteroLista, NULL
            je finlistPrint
            mov rsi, coma
            call fprintf
            jmp ciclolistPrint
        finlistPrint:
        mov rdi, archivo
        mov rsi, corchete2
        call fprintf
        add rsp, 8
        pop rbx
        pop r13 
        pop r12
        pop rbp
        ret


FuncionesSorter:

    %define offset_size 0
    %define offset_sorterFun 8
    %define offset_cmpFun 16
    %define offset_slots 24
    sorterNew:
        push rbp
        mov rbp, rsp
        push rbx
        push r12
        push r13
        push r14
        push r15
        sub rsp, 8

        %define sizeSorter bx
        %define fun_sor r13
        %define fun_cmp r14
        %define slots r15
        %define sorterNew r12

        mov rbx, 0
        mov sizeSorter, di 
        mov fun_sor, rsi
        mov fun_cmp, rdx
        lea rdi, [rbx *8]
        call malloc
        mov slots, rax

        mov rdi, 32
        call malloc
        mov [rax + offset_size], sizeSorter
        mov [rax + offset_sorterFun], fun_sor
        mov [rax + offset_cmpFun], fun_cmp
        mov [rax + offset_slots], slots

        mov sorterNew, rax
        mov r13, 0
        cicloSorterNew:
            cmp r13, rbx
            je finSorterNew
            
            call listNew  
            mov [slots + r13 *8], rax

            inc r13
            jmp cicloSorterNew
        finSorterNew:
        mov rax, sorterNew
        add rsp, 8
        pop r15
        pop r14
        pop r13
        pop r12
        pop rbx
        pop rbp
        ret
         
    sorterAdd:
        push rbp
        mov rbp, rsp
        push r12
        push r15

        %define data r12
        %define sorter r15

        mov sorter, rdi
        mov data, rsi

        mov rdi, data  
        call [sorter + offset_sorterFun]
        and rax, 0x0000FFFF

        mov rdi, [sorter + offset_slots] 
        mov rdi, [rdi + rax*8]
        mov rsi, data
        mov rdx, [sorter + offset_cmpFun]
        call listAdd 
        
        
        pop r15
        pop r12
        pop rbp
        ret
    
    sorterRemove:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push r15
        sub rsp, 8

        %define data r12
        %define funcionQueBorra r13
        %define sorter r15


        mov sorter, rdi
        mov data, rsi
        mov funcionQueBorra, rdx

        mov rdi, data  
        call [sorter + offset_sorterFun]

        mov rdi, [sorter + offset_slots] 
        mov rdi, [rdi + rax*8]
        mov rsi, data
        mov rdx, [sorter + offset_cmpFun]
        mov rcx, funcionQueBorra
        call listRemove 
        
       
        add rsp, 8
        pop r15
        pop r13
        pop r12
        pop rbp
        ret
    
    sorterGetSlot:
        push rbp
        mov rbp, rsp

        %define indiceSlot rsi ;viene en si
        %define funcionQueDup rdx
        %define sortr rdi

        AND indiceSlot, 0x000000FF 
        
        mov rdi, [sortr + offset_slots] 
        mov rdi, [rdi + indiceSlot*8]

        mov rsi, funcionQueDup
        
        call listClone 
        ;el puntero a la lista clonada viene en rax entonces no hace falta hacer mas nada

        pop rbp
        ret
    
    
    
    
    ;Dado un slot, obtiene una string que concatena todos los elementos de la lista de forma ordenada.
    ;char* sorterGetConcatSlot(sorter_t* sorter, uint16_t slot);
    sorterGetConcatSlot:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push r15
        sub rsp, 8

        
        %define nodoCopy r13
        %define sizeStr r12
        %define slotCopy r15


        ;en la parte baja de rsi viene el slot a copiar
        AND rsi, 0x000000FF
        ; en r13 pongo el slot que hay que concaternar
        mov rcx, [rdi + offset_slots] 
        mov slotCopy, [rcx + rsi*8]

        
        ;inicializo un contador
        mov sizeStr, 0
        mov nodoCopy, [slotCopy + offset_first]
        cicloContar:
            cmp nodoCopy, NULL 
            je crearString
            mov rdi, [nodoCopy + offset_data]
            call strLen
            add sizeStr, rax
            mov nodoCopy, [nodoCopy + offset_next]
            jmp cicloContar
        crearString:
        ;ya tengo el tamaño para el string, lo pongo en rdi y pido la memoria
        lea rdi, [sizeStr+1]
        call malloc 
        ;en rax tengo el principio del str nuevo
        mov nodoCopy, [slotCopy + offset_first]
        mov rcx, 0
        cicloCopiarStr:
            cmp nodoCopy, NULL 
            je finSorterGetConcatSlot
            mov r8, [nodoCopy + offset_data]
            mov rdi, 0
                ciclostrIesimo:
                CMP byte [r8 + rdi * sizeChar], 0
                JE  sigStr
                MOV dl, [r8 + rdi * sizeChar]
                MOV [rax + rcx * sizeChar], dl
                INC rdi
                INC rcx
                JMP ciclostrIesimo
            sigStr:
            mov nodoCopy, [nodoCopy + offset_next]
            jmp cicloCopiarStr
        finSorterGetConcatSlot:  
        mov byte [rax + rcx * sizeChar], 0

        add rsp, 8
        pop r15
        pop r13
        pop r12
        pop rbp
        ret
    
    
    sorterCleanSlot:
        push rbp
        mov rbp, rsp
        push r13
        push r15
                
        %define indexSlot r13
        %define sorter r15

        
        AND rsi, 0x000000FF
        mov indexSlot, rsi
        mov sorter, rdi

        mov rdi, [rdi + offset_slots] 
        mov rdi, [rdi + rsi*8] ;pongo la lista
        mov rsi, rdx ;pongo la funcion que borra
        call listDelete 
        
        call listNew
        mov rdi, [sorter + offset_slots]
        mov [rdi + indexSlot*8], rax

        pop r15
        pop r13
        pop rbp
        ret
    sorterDelete:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push rbx
        push r15
        
        %define funcionQueBorra r13
        %define sorter r15
        %define sizeS rbx
        %define indiceActual r12

        mov funcionQueBorra, rsi
        mov sorter, rdi
        mov rbx, 0
        mov bx, [sorter+offset_size]
        mov indiceActual, 0
        cicloSlots:
            cmp indiceActual, sizeS
            je finSorterDelete
            mov rdi, [sorter+offset_slots]
            mov rdi, [rdi+indiceActual*8]
            mov rsi, funcionQueBorra
            call listDelete 
            inc indiceActual
            jmp cicloSlots   
        finSorterDelete:
        mov rdi, [sorter+offset_slots]
        call free 
        mov rdi, sorter
        call free
       
        pop r15
        pop rbx
        pop r13
        pop r12
        pop rbp
        ret
    sorterPrint:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push r14
        push r15
        
        %define funcionQuePrint r13
        %define sorter r15
        %define archivo r14
        %define indiceActual r12

        mov funcionQuePrint, rdx
        mov sorter, rdi
        mov archivo, rsi
        
        mov indiceActual, 0
        ciclosorterPrint:
            mov rdx, 0
            mov dx, [sorter+offset_size]
            cmp indiceActual, rdx
            je finSorterPrint
	        mov rdi, archivo
            mov rsi, formatoIndice
            mov rdx, indiceActual
	        call fprintf

            mov rdi, [sorter+offset_slots]
            
            mov rdi, [rdi+indiceActual*8]
            mov rsi, archivo
            mov rdx, funcionQuePrint
            call listPrint
            mov rdi, archivo
            mov rsi, endLine
            call fprintf
            inc indiceActual
            jmp ciclosorterPrint
        finSorterPrint:
        pop r15
        pop r14
        pop r13
        pop r12
        pop rbp
        ret

FuncionesAux:
    fs_sizeModFive:
        push rbp
        mov rbp, rsp
        
        ;como a viene en dri puedo pedir directamente su longitud
        call strLen
	
        mov rdx, 0
        and rax, 0x0000FFFF
        mov esi, 5
        div esi
        ;aca hay que hacer cosas con edx que teine el resto de divir eax con 5

        mov ax, dx
        pop rbp
        ret    
    fs_firstChar:
        mov rax, 0
        MOV al, [rdi+  0 * sizeChar]
        ret
       
    fs_bitSplit:
      
        push rbp
        mov rbp, rsp
       
        ;busco el primer char, ya esta en rdi asique solo llamo a la funcion
        mov rax, 0
        MOV al, [rdi]
     
        ;Tengo el resultado en hexa en al

        cmp al, 00h
        je .ret8
        cmp al, 01h
        je .ret0
        cmp al, 02h
        je .ret1
        cmp al, 04h
        je .ret2
        cmp al, 08h
        je .ret3
        cmp al, 10h
        je .ret4
        cmp al, 20h
        je .ret5
        cmp al, 40h
        je .ret6
        cmp al, 80h
        je .ret7
        mov ax, 9
        jmp .fin

        .ret0:
        mov ax, 0
        jmp .fin
        .ret1:
        mov ax, 1
        jmp .fin

        .ret2:
        mov ax, 2
        jmp .fin

        .ret3:
        mov ax, 3
        jmp .fin

        .ret4:
        mov ax, 4
        jmp .fin

        .ret5:
        mov ax, 5
        jmp .fin

        .ret6:
        mov ax, 6
        jmp .fin

        .ret7:
        mov ax, 7
        jmp .fin

        .ret8:
        mov ax, 8
        jmp .fin

        .fin:
        pop rbp
        ret

